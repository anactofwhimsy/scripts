#!/bin/bash

# Check if all arguments are provided
if [ $# -ne 2 ]; then
    echo "Usage: $0 <category> <destination_directory>"
    exit 1
fi

# Assign arguments to variables
category="$1"
dest_dir="$2"
search_dir="$HOME/Projects/handbooks/handbook/content/handbook/support/workflows"

# Check if the search directory exists
if [ ! -d "$search_dir" ]; then
    echo "Error: Search directory '$search_dir' does not exist."
    exit 1
fi

# Create the destination directory if it doesn't exist
mkdir -p "$search_dir/$dest_dir"

# Search for files containing the category and move them
found_files=0
while IFS= read -r -d '' file; do
    #echo "Checking file: $file"
    if grep -qi "category.*$category" "$file"; then
        #echo "Found match in file: $file"
        mv "$file" "$search_dir/$dest_dir/"
        #echo "Moved: $file"
        ((found_files++))
    fi
done < <(find "$search_dir" -type f -name "*.md" -print0)


# Print summary
if [ $found_files -eq 0 ]; then
    echo "No files found containing the category '$category'."
else
    echo "Moved $found_files file(s) to $dest_dir"
fi
