#!/bin/bash

# Array of urls / paths to search
urls=(
    "handbook/support.*/#raising-an-emergency" 
    "handbook/support.*/#raising-us-government-support-emergencies" 
    "handbook/support.*/#dormant-namespace-contact" 
    "handbook/support.*/#special-circumstances" 
    "handbook/support.*/#data-restore-requests-exemptions" 
    "handbook/support.*/#phone-number-verification-issues" 
    "handbook/support.*/#exemption-requests" 
    "handbook/support.*/#telesign-outages" 
    "handbook/support.*/#rate-limits" 
    "handbook/support.*/#email-address-verification-issues" 
    "handbook/support.*/#check-verification-email-last-sent-time" 
    "handbook/support.*/#check-verification-email-delivery-status" 
    "handbook/support.*/#one-time-email-address-update" 
    "handbook/support.*/#support-uploader" 
    "handbook/support.*/#uploading-a-file-via-cli" 
    "handbook/support.*/#download-a-file-via-cli" 
)

# Directory to search
search_dir="/Users/iblackburn/Projects/handbooks"

# Loop through each text and search
for url in "${urls[@]}"; do
 #   result=$(grep -r -l --include="*" "$text" "$search_dir")
    result=$(ack --files-with-matches -c "$url" "$search_dir")
    #result=$(ack "$url" "$search_dir")
    if [ -n "$result" ]; then
        echo 
        echo "Mentions of $url:"
        while read -r line; do
            echo "* $line"
        done <<< "$result"

        echo
#    else
#        echo "No matches found for '$url'"
    fi
done
