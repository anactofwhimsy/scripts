import os
import subprocess
from urllib.parse import unquote

def get_moved_files(repo_path):
    """Get a list of moved files from the latest commit in the specified repository."""
    cmd = ["git", "-C", repo_path, "diff-tree", "--no-commit-id", "--name-status", "-r", "--find-renames=40%", "HEAD~1", "HEAD"]
    result = subprocess.run(cmd, capture_output=True, text=True)
    moved_files = []
    for line in result.stdout.splitlines():
        if line.startswith('R'):
            _, old_path, new_path = line.split('\t')
            moved_files.append((old_path, new_path))
    return moved_files

def generate_ref_formats(path):
    """Generate possible reference formats for a given path."""
    formats = []
    # Full path
    formats.append(path)
    # Path without extension
    formats.append(os.path.splitext(path)[0])
    # Filename with extension
    formats.append(os.path.basename(path))
    # Filename without extension
    formats.append(os.path.splitext(os.path.basename(path))[0])
    # URL encoded path
    formats.append(unquote(path))
    # Replace forward slashes with backslashes and vice versa
    formats.append(path.replace('/', '\\'))
    formats.append(path.replace('\\', '/'))
    return formats

def find_references(repo_path, old_path):
    """Find references to the old file path in all files within the specified repository."""
    references = []
    ref_formats = generate_ref_formats(old_path)
    for root, _, files in os.walk(repo_path):
        for file in files:
            if file.endswith(('.md', '.txt', '.html')):  # Add more file types if needed
                file_path = os.path.join(root, file)
                with open(file_path, 'r', encoding='utf-8') as f:
                    for line_number, line in enumerate(f, 1):
                        for ref_format in ref_formats:
                            if ref_format in line:
                                references.append((file_path, line_number, line.strip(), ref_format))
                                break
    return references

def process_repositories(repositories):
    # Get moved files from the first repository
    main_repo = repositories[0]
    print(f"\nChecking for moved files in: {main_repo}")
    moved_files = get_moved_files(main_repo)
    if not moved_files:
        print("No files were moved in the latest commit.")
        return

    print("Files moved in the latest commit:")
    for old_path, new_path in moved_files:
        print(f"{old_path} -> {new_path}")

    # Find references in all repositories
    for repo_path in repositories:
        print(f"\nSearching for references in: {repo_path}")
        for old_path, new_path in moved_files:
            references = find_references(repo_path, old_path)
            if references:
                print(f"\nReferences to {old_path}:")
                for file_path, line_number, line, ref_format in references:
                    print(f"  {file_path}:{line_number}: {line}")
                    print(f"    Matched format: {ref_format}")
            else:
                print(f"No references to {old_path} were found.")

def main():
    import sys
    if len(sys.argv) < 2:
        print("Usage: python script.py <repo1_path> [<repo2_path> ...]")
        sys.exit(1)
    
    repositories = sys.argv[1:]
    process_repositories(repositories)

if __name__ == "__main__":
    main()
